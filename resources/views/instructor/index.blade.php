@extends('instructor.master')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">

        @if (Session::has('success'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong> {{ session::get('success')}}</strong>
          </div>
        @endif
        
            <h1 class="title">Hey, I'm Instructor Dashboard!</h1>
            Instructor login name: {{ Auth::guard('instructor')->user()->name }} <br>
            Instructor Email Id: {{ Auth::guard('instructor')->user()->email }} <br>

            <a href="{{ route('instructor_logout') }}" class="dashboard-nav-item"><i class="fas fa-sign-out-alt"></i> Logout </a>

        </div>
    </div>
</div>

@endsection