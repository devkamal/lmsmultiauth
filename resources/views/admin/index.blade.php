@extends('admin.master')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">

        @if (Session::has('error'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong> {{ session::get('error')}}</strong>
          </div>
        @endif
        
            <h1 class="title">Hey, I'm Admin Dashboard!</h1>
            Admin login name: {{ Auth::guard('admin')->user()->name }} <br>
            Admin Email Id: {{ Auth::guard('admin')->user()->email }} <br>

            <a href="{{ route('admin_logout') }}" class="dashboard-nav-item"><i class="fas fa-sign-out-alt"></i> Logout </a>

        </div>
    </div>
</div>

@endsection