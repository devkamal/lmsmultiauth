<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\InstructorController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

//******==================Start of Admin Controller***********/
Route::prefix('admin')->group(function(){
    Route::get('/login', [AdminController::class, 'Index'])->name('login_form');
    Route::post('/login/owner', [AdminController::class, 'Login'])->name('admin_login');
    Route::get('/dashboard', [AdminController::class, 'Dashboard'])->name('admin_dashboard')->middleware('admin');
    Route::get('/logout', [AdminController::class, 'adminLogout'])->name('admin_logout');

    Route::get('/register', [AdminController::class, 'adminRegister'])->name('admin_register');
    Route::post('/register/store', [AdminController::class, 'adminStore'])->name('admin_register_store');
});
//******==================End of Admin Controller***********/

//******==================Start of Instructor Controller***********/
Route::prefix('instructor')->group(function(){
    Route::get('/login', [InstructorController::class, 'Index'])->name('instructor_login_form');
    Route::post('/login/owner', [InstructorController::class, 'Login'])->name('instructor_login');
    Route::get('/dashboard', [InstructorController::class, 'Dashboard'])->name('instructor_dashboard')->middleware('instructor');
    Route::get('/logout', [InstructorController::class, 'instructorLogout'])->name('instructor_logout');

    Route::get('/register', [InstructorController::class, 'instructorRegister'])->name('instructor_register');
    Route::post('/register/store', [InstructorController::class, 'instructorStore'])->name('instructor_register_store');
});
//******==================End of Instructor Controller***********/
