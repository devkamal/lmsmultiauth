<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;

class Admin
{
  

    public function handle(Request $request, Closure $next)
    {
        if(!Auth::guard('admin')->check()){
            return redirect()->route('login_form')->with('error','অ্যাডমিন সাহেব আগে লগইন করুন !');
        }else{
            
        }
        
        return $next($request);

    
    }
}
