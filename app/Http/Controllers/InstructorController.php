<?php

namespace App\Http\Controllers;

use App\Models\Instructor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use Carbon\Carbon;

class InstructorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function Index()
    {
        return view('instructor.login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function Login(Request $request)
    {
        // dd($request->all());

        $check = $request->all();
        if(Auth::guard('instructor')->attempt( ['email' => $check['email'], 'password' => $check['password'] ] ) ){
            return redirect()->route('instructor_dashboard')->with('success','Instructor login success');
        }else{
            return back()->with('error','Invalid Email or Password');
        }

    }

    public function Dashboard(){
        return view('instructor.index');
    }


    public function instructorLogout()
    {
        Auth::guard('instructor')->logout();
        return redirect()->route('instructor_login_form')->with('success','Instructor logout success');
    }

    //Start of Instructor registration portion
   public function instructorRegister(){
        return view('instructor.register.register_form');
   }

   public function instructorStore(Request $request){
        $validateData = $request->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
        ]);

        Instructor::insert([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'created_at' => Carbon::now(),
        ]);
        return redirect()->route('instructor_login_form')->with('success','Instructor registered success');
   }


}
